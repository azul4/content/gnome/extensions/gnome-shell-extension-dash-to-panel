# gnome-shell-extension-dash-to-panel

[![License: GPL v2](https://img.shields.io/badge/License-GPL%20v2-blue.svg)](https://img.shields.io/badge/License-GPL%20v2-blue.svg)

Extension for GNOME shell to combine the dash and main panel

https://github.com/home-sweet-gnome/dash-to-panel

<br><br>
How to clone this repository:

```
git clone https://gitlab.com/azul4/content/gnome/extensions/gnome-shell-extension-dash-to-panel.git
```
